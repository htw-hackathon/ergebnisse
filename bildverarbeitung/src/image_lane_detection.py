#!/usr/bin/env python

import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import PointCloud2, PointField
import numpy as np

from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import math

out = 0
street = 0

def callback(data):

    # getting birds eye view of camera and extracting the contours
    bridge = CvBridge()
    cv_image = bridge.imgmsg_to_cv2(data, "bgr8")

    cv_image = cv_image[500:910, 0:1919]    # constans for rollei to get a birds eye image
    origin = np.float32([[744,106],[1160,111],[761,267],[1125,273]])
    target = np.float32([[49,100],[1051,100],[387,1000],[714,1000]])

    # cv_image = cv_image[500:910, 0:1919]    # constans for axis_front to get a birds eye image
    # origin = np.float32([[744,106],[1160,111],[761,267],[1125,273]])
    # target = np.float32([[49,100],[1051,100],[387,1000],[714,1000]])

    m = cv2.getPerspectiveTransform(origin,target)
    cv_image = cv2.warpPerspective(cv_image,m,(1100,1100))

    cv_image = cv2.rotate(cv_image, 2)

    contoursimg = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)    # extracting the contours
    _, thresh = cv2.threshold(contoursimg, 185, 255, cv2.THRESH_BINARY)

    _, contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )

    '''
    newContours = []

    for c in contours:
        if len(c) > 100:
            newC = []
            i = 0
            for p in c:
                if i%2 == 0:
                    newC.append([p[0][0], p[0][1]])
                    #cv2.circle(cv_image,(p[0][0], p[0][1]), 5, (0,255,0), -1)
                i += 1

            newContours.append(newC)
    '''



    #cv2.drawContours(cv_image, newContours, -1, (0,255,0), 3)


    #contours are put in marker message

    vel = Marker()
    vel.header.frame_id = "odom"
    vel.header.stamp = rospy.Time.now()
    vel.ns = "points"
    vel.action = Marker.ADD
    vel.type = Marker.POINTS

    vel.pose.orientation.w = 1
    vel.scale.x = 0.2
    vel.scale.y = 0.2

    vel.color.a = 1.0
    vel.color.g = 1.0


    newContours = []

    for c in contours:
        if len(c) > 100:
            i = 0
            for p in c:
                if i%2 == 0:
                    vel.points.append(Point((1100-p[0][0])*0.015, (p[0][1]-550)*0.015,0))

                i += 1


    # vel.points = newContours

    street.publish(bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    out.publish(vel)


def main():

    rospy.init_node('image_lane_detection', anonymous=False)
    rospy.loginfo('image_lane_detection node initialized')
    global out
    global street
    street = rospy.Publisher('/camera/street', Image, queue_size=1)
    out = rospy.Publisher('/marker/street', Marker, queue_size=1)

    rospy.Subscriber('/camera/image_rect', Image, callback)

    #rate = rospy.rate(5)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()





if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
