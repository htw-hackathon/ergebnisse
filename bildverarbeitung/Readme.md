# HTW - Hackathon
## Image Lane Detection
### To rectify the image use
* ROS_NAMESPACE=my_camera rosrun image_proc image_proc
* replace my_camera with the camera you want to rectify

#### Run the node with 
* rosrun camera_segmentation image_lane_detection.py

### Installation
Download the source in your /catkin/src directory and run catkin_make 

### Dependencies
-cv2

### Synopsis
Extracting lane marking points via cv2 contour extraction

The algorithm
* uses camera_info to rectify the image
* uses measured points to create a birds eye view of the lane
* extracts the contour points from the image


* subscribed topic:
    + /camera/image_rect #rectified image of specified camera       
 

* published topics:
    + /camera/street #birds eye view
    + /marker/street #recognised street markers
    
### Hints / TodDos 
* improve scaling of the markers
* improve performance
