# HTW - Hackathon
 
## Lane detection and trajectory point extraction by team 2
Mariia,Karl,Friedrich,Nick

### Synopsis
Extracting surface and lane marking points from a lidar scan relative to the vehicle position 

The algorithm
 * callback filters raw pointcloud data from lidar to small amount of points and publishes them
 * transforms the raw data from diagonal to a horizontal level
 * apply a intensity filter to get pointcloud with points at a high intensity
 * apply a heightfilter to only look at a certain amount of vertical levels
 * build clusters with euclideanclusterextraction
 * take the cluster with maximum amoint of points to extract outer lane
 * divide maximum cluster into 5 areas of pointclouds depending on distance
 * take 5 average points from that areas
 * add a Car Offset to the points to move them in the right lane for driving


* subscribed topic:
    + /points_raw   #ouster points           
 

* published topics:
    + /path #points for pathfinding
    + /cloud_seg/lane #segmentated point cloud lane points 
   
### Motivation
Creating an lane assist based on lidar information 
  

### Installation
Download the source in your /catkin/src directory and run catkin_make 

###Run nodes with:   
`rosrun lane_detection final_lane_node ` <br> and `rosrun lane_detection lane_detection_node` </br>

### Tests
some, working for several consecutive rounuds

