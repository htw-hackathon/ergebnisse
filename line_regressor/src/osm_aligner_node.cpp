//
// Created by htwk-smart-driving on 2019/08/07.
//

#include <ros/init.h>
#include "OSMAligner.h"

#define SOURCE_FRAME "ouster"
#define TARGET_FRAME "map"

int main(int argc, char **argv) {
    ros::init(argc, argv, "osm_aligner");
    ros::NodeHandle nh;

    OSMAligner node = OSMAligner(nh);

    ros::Rate loop_rate(10);
    while (ros::ok()) {
        try {
            node.offsetListener->lookupTransform(TARGET_FRAME, SOURCE_FRAME, ros::Time(0), node.offset);
            node.calculateOffset();
        }
        catch (tf::TransformException te) {
            ROS_ERROR("%s", te.what());
        }

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}