//
// Created by htwk-smart-driving on 2019/08/08.
//

#include <ros/init.h>
#include <visualization_msgs/MarkerArray.h>
#include "LineRegressor.h"
#include "osm_aligner_helper.cpp"

#define linearInterpolation(x1, y1, x2, y2, x) ((y1*(x2 - x) + y2*(x - x1)) / (x2 - x1))

void callback(const visualization_msgs::MarkerArrayConstPtr &markerArray) {
//    auto filteredMarkers = osm_aligner_helper::filterMarkers(*markerArray);
//
//    auto newMarkerArray = new visualization_msgs::MarkerArray();
//    int arrayIndex = 0;
//
//    for (visualization_msgs::Marker marker : filteredMarkers.markers) {
//        if (marker.type == visualization_msgs::Marker::LINE_LIST) {
//            for (int i = 1; i < marker.points.size(); i++) {
//                auto point1 = marker.points[i - 1];
//                auto point2 = marker.points[i];
//
//                auto d = std::sqrt(std::pow(point1.x - point2.x, 2) + std::pow(point1.y - point2.y, 2));
//                newMarkerArray->markers[arrayIndex].points.insert(newMarkerArray->markers[arrayIndex].points.end(), point1);
//                while (d > 2) {
//
//                    d--;
//                }
//                newMarkerArray->markers[arrayIndex].points.insert(newMarkerArray->markers[arrayIndex].points.end(), point2);
//            }
//            arrayIndex++;
//        }
//    }
}

void addPointsToLineList(visualization_msgs::Marker *marker) {

}


int main(int argc, char **argv) {
    ros::init(argc, argv, "map_filter");
    ros::NodeHandle nodeHandle;

    ros::Subscriber subscriber = nodeHandle.subscribe("/osm/marker", 1, callback);
    ros::Publisher publisher;


    ros::Rate loop_rate(1);
    while (ros::ok()) {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

