//
// Created by htwk-smart-driving on 2019/08/07.
//

#ifndef HTW_HACKATHON_LINEREGRESSOR_H
#define HTW_HACKATHON_LINEREGRESSOR_H


#include <ros/node_handle.h>
#include <sensor_msgs/PointCloud2.h>

class LineRegressor {
public:
    explicit LineRegressor(ros::NodeHandle);
    void callback(const sensor_msgs::PointCloud2ConstPtr &cloud_msg);

private:

    const std::string NODE_NAME = "line_regressor";
    const std::string PUBLISHED_TOPIC_NAME = NODE_NAME + "/regressed_function";
    const std::string SUBSCRIBED_TOPIC_NAME = "LaserFilterNode/reflexSpurrPunkte";

    ros::NodeHandle nodeHandle;
    ros::Publisher publisher;
    ros::Subscriber subscribedTo;

};


#endif //HTW_HACKATHON_LINEREGRESSOR_H
