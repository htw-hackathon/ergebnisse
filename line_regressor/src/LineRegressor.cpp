//
// Created by htwk-smart-driving on 2019/08/07.
//

#include "LineRegressor.h"

LineRegressor::LineRegressor(ros::NodeHandle) {
    this->subscribedTo = nodeHandle.subscribe(LineRegressor::SUBSCRIBED_TOPIC_NAME, 1, &LineRegressor::callback, this);
//    this->publisher = nodeHandle.advertise(LineRegressor::PUBLISHED_TOPIC_NAME, 1); // todo type of message
}

void LineRegressor::callback(const sensor_msgs::PointCloud2ConstPtr &cloud_msg) {
    ROS_DEBUG("Hello %s", "World");
}
