//
// Created by htwk-smart-driving on 2019/08/08.
//

#include <pcl/point_cloud.h>
#include <tf/LinearMath/Vector3.h>
#include <pcl_ros/transforms.h>
#include <visualization_msgs/MarkerArray.h>
#include <pcl_conversions/pcl_conversions.h>

namespace osm_aligner_helper {
    const int VICINITY_RADIUS = 10;

    visualization_msgs::MarkerArray filterMarkers(visualization_msgs::MarkerArray markerArrayUnfiltered) {
        // better: use pcl::PassThrough
        visualization_msgs::MarkerArray markerArrayFiltered = visualization_msgs::MarkerArray();
        std::copy_if(markerArrayUnfiltered.markers.begin(), markerArrayUnfiltered.markers.end(),
                     std::back_inserter(markerArrayFiltered.markers),
                     [](visualization_msgs::Marker marker) { return marker.ns == "osm_roadmarking"; });

        return markerArrayFiltered;
    }

    pcl::PointCloud<pcl::PointXYZ>
    getMapDataInVicinity(tf::Vector3 offsetVector, visualization_msgs::MarkerArray markerArray) {
        pcl::PointCloud<pcl::PointXYZ> mapData;
        mapData.header.frame_id = "map";
        pcl_conversions::toPCL(ros::Time::now(), mapData.header.stamp);

        for (visualization_msgs::Marker marker : markerArray.markers) {
            // todo add points in between long lines
            for (geometry_msgs::Point point : marker.points) {
                float dx = point.x - offsetVector.x();
                float dy = point.y - offsetVector.y();
                float euclideanDistance = sqrt(dx * dx + dy * dy);

                if (euclideanDistance < VICINITY_RADIUS) {
                    auto pointConverted = new pcl::PointXYZ(point.x, point.y, point.z);
                    mapData.insert(mapData.end(), *pointConverted);
                }
            }
        }
        return mapData;
    }
}