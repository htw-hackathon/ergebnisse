//
// Created by htwk-smart-driving on 2019/08/07.
//

#include <ros/init.h>
#include "LineRegressor.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "line_regressor");
    ros::NodeHandle nh;

    LineRegressor node = LineRegressor(nh);

    ros::Rate loop_rate(10);
    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}