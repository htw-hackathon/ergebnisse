//
// Created by htwk-smart-driving on 2019/08/07.
//

#ifndef HTW_HACKATHON_OSMALIGNER_H
#define HTW_HACKATHON_OSMALIGNER_H


#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <pcl_conversions/pcl_conversions.h>

class OSMAligner {
public:
    tf::TransformListener *offsetListener;
    tf::StampedTransform offset;

    explicit OSMAligner(ros::NodeHandle);

    void calculateOffset();
    void callbackOSM(const visualization_msgs::MarkerArrayConstPtr &markerArray);
    void callbackLidar(const sensor_msgs::PointCloud2ConstPtr &pointCloud);

private:
    const std::string MAP_HEADER_ID = "map";
    const std::string NODE_NAME = "osm_aligner";
    const std::string OWN_TOPIC_NAME = NODE_NAME + "/filtered_ns";
    const std::string TOPIC_NAME_OSM_MARKER = "/osm/marker";
    const std::string TOPIC_NAME_LIDAR = "/points_raw";
    const std::string OSM_NAMESPACE = "osm_roadmarking";

    ros::NodeHandle nodeHandle;
    ros::Subscriber osmSubscription, lidarSubscription;
    ros::Publisher publisher;
    visualization_msgs::MarkerArray mapData;
    sensor_msgs::PointCloud2 lidarData;
    tf::TransformListener *transformListener;

    void updateMapCoordinateSystem(tf2::Vector3 translation, tf2::Quaternion rotation);
    pcl::PointCloud<pcl::PointXYZI> convertToPointCloud(const sensor_msgs::PointCloud2 pointCloud) const;
};


#endif //HTW_HACKATHON_OSMALIGNER_H
