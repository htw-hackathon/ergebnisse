//
// Created by htwk-smart-driving on 2019/08/07.
//

#include <visualization_msgs/MarkerArray.h>
#include <tf2_ros/transform_broadcaster.h>
#include <pcl_ros/transforms.h>
#include "OSMAligner.h"
#include "osm_aligner_helper.cpp"

OSMAligner::OSMAligner(ros::NodeHandle nh) : nodeHandle(nh) {
    this->transformListener = new tf::TransformListener();
    this->offsetListener = new tf::TransformListener();
    this->publisher = nodeHandle.advertise<sensor_msgs::PointCloud2>(OSMAligner::OWN_TOPIC_NAME, 1);
    this->osmSubscription = nodeHandle.subscribe(TOPIC_NAME_OSM_MARKER, 1, &OSMAligner::callbackOSM, this);
    this->lidarSubscription = nodeHandle.subscribe(TOPIC_NAME_LIDAR, 1, &OSMAligner::callbackLidar, this);
}

void OSMAligner::callbackOSM(const visualization_msgs::MarkerArrayConstPtr &markerArray) {
    this->mapData = osm_aligner_helper::filterMarkers(*markerArray);
}

void OSMAligner::callbackLidar(const sensor_msgs::PointCloud2ConstPtr &pointCloud) {
    this->lidarData = *pointCloud;

    // ROS_INFO_STREAM_COND("%s\n", pointCloud.get()->header.frame_id);
//    sensor_msgs::PointCloud2 tempCloud;
//    pcl_ros::transformPointCloud(MAP_HEADER_ID, *pointCloud, tempCloud, *this->transformListener);
//    pcl::PointCloud<pcl::PointXYZI> cloud = convertToPointCloud(tempCloud);
//
//    //cloud.points
//    ROS_INFO_STREAM_COND("%d\n",cloud.points[0].x);

}

void OSMAligner::calculateOffset() {
    ROS_INFO_STREAM("calculating offset");
    auto lidarPoints = OSMAligner::convertToPointCloud(this->lidarData);
    auto offsetVector = this->offset.getOrigin();
    auto mapPoints = osm_aligner_helper::getMapDataInVicinity(offsetVector, this->mapData);


    pcl::PCLPointCloud2 outputPCL;
    sensor_msgs::PointCloud2 output;

    pcl::toPCLPointCloud2(mapPoints, outputPCL);
    pcl_conversions::fromPCL(outputPCL, output);
    publisher.publish(output);

    ROS_INFO_STREAM_COND("%d", mapPoints.points.size());
    // todo offset finder

}

pcl::PointCloud<pcl::PointXYZI> OSMAligner::convertToPointCloud(const sensor_msgs::PointCloud2 pointCloud) const {
    auto *pclCloud2 = new pcl::PCLPointCloud2();
    pcl_conversions::toPCL(pointCloud, *pclCloud2);

    // cresting  boost shared pointer for pcl function inputs
    pcl::PointCloud<pcl::PointXYZI> *inputCloud = new pcl::PointCloud<pcl::PointXYZI>;
    pcl::PointCloud<pcl::PointXYZI>::Ptr inputCloudPtr(inputCloud);
    // convert the pcl::PointCloud2 tpye to pcl::PointCloud<pcl::PointXYZI>
    pcl::fromPCLPointCloud2(*pclCloud2, *inputCloudPtr);

    return *inputCloud;
}

void OSMAligner::updateMapCoordinateSystem(tf2::Vector3 translation, tf2::Quaternion rotation) {
    static tf2_ros::TransformBroadcaster broadcaster;
    geometry_msgs::TransformStamped transformStamped;

    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = MAP_HEADER_ID;
    transformStamped.child_frame_id = MAP_HEADER_ID + "_corr";
    transformStamped.transform.translation.x = translation.x();
    transformStamped.transform.translation.y = translation.y();
    transformStamped.transform.translation.z = translation.z();

    transformStamped.transform.rotation.x = rotation.x();
    transformStamped.transform.rotation.y = rotation.y();
    transformStamped.transform.rotation.z = rotation.z();
    transformStamped.transform.rotation.w = rotation.w();

    broadcaster.sendTransform(transformStamped);

}

