#  HTW -  Hackathon
## Lidar Segmentation Example 
 
### Version 
0.1
 
### Author 
Sven Eckelmann (eckelmann@htw-dresden.de)<br>
adapted by Hackathon Team1 </br>

### Synopsis
Extracting surface and lane marking points from a lidar scan relative to our vehicle position 

The algorithm
* uses pass through filter in z direction 
* applies a ransac to extract only surface points and 
* uses different pass through for intensity to extract reflecting points 


* subscribed topic:
    + /points_raw   #ouster points           
 

* published topics:
    + /cloud_seg/ground    #represents the surface 
    + /cloud_seg/lane      #represents the  lane marking points  
    + /cloud_seg/not_segmented          #all non segmented points
    + /LaserFilterNode/alleSpurPunkte   #lane marking points 
    + /LaserFilterNode/reflexSpurPunkte #only high reflectivity lane points 
   
### Motivation
Creating an lane assist based on lidar information 

### Dependencies:
- pcl
- eigen3


### Installation
Download the source in your /catkin/src directory and run catkin_make 

Run node with:   
`rosrun lidar_segementation_example lidar_segementation_example_node `
works with spurerkennung package

### Hints / TodDos 
* remove static threshold for pass through intensity filter 

### Tests
not done ... 

 

    
