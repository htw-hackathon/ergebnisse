#!/usr/bin/env python

import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs.point_cloud2 as pc2
from nav_msgs.msg import Path

out = 0


def callback(data):
    #putting high reflectivity points into xs and ys
    point_generator = pc2.read_points(data, skip_nans=True, field_names=("x", "y"))
    xs = []
    ys = []
    for p in point_generator:
        xs = xs + [p[0]]
        ys = ys + [p[1]]

    #filtering out points with a z_score < threshold for the standard deviation on the y axis
    ysaverage = np.mean(ys)
    ysstd = np.std(ys)
    lng = len(xs)
    threshold = 3
    xsf = []			
    ysf = []
    for i in range(0, lng):
        z_score = (ys[i] - ysaverage) / ysstd
        if np.abs(z_score) < threshold:
            xsf.append(xs[i])
            ysf.append(ys[i])

    #fits a polynomial onto the points and filters out unrealistic curves
    poly = np.polyfit(xsf, ysf, 2)
    if poly[0] > 0.1 or poly[0] < -0.1:
        print ("Polynomial curvature too high, choosing last poly")
        return
    
    #puts the polynomial onto the middle of the road with a simple x axis offset
    spur_offset = 3.35
    if poly[2] > 0:
        poly[2] = poly[2] - spur_offset
    else:
        poly[2] = poly[2] + spur_offset


    #gives a number of points in plot on a certain range in front of the vehicle
    plot_length_m = -106.4
    plot_length_n = 14.32
    plot_to = poly[0] * plot_length_m + plot_length_n
    if plot_to > 14:
        plot_to = 14
    if plot_to < 10:
        plot_to = 10

    plot_step = 2.5
    plot_from = 4
    plot = []

    for i in range(int((plot_from / plot_step)), int((plot_to / plot_step))):
        x = i * plot_step
        plot = plot + [np.polyval(poly, x)]

    #publishes the points as a Path
    output = Path()
    output.header.stamp = rospy.get_rostime()
    output.header.frame_id = 'odom'

    x = plot_from
    for y in plot:
        pose = PoseStamped()
        pose.header = output.header
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = 0
        pose.pose.orientation.x = 0
        pose.pose.orientation.y = 0
        pose.pose.orientation.z = 0
        pose.pose.orientation.w = 1
        output.poses.append(pose)
        x += (1 * plot_step)

    out.publish(output)


def main():
    print("start")

    rospy.init_node('polynode', anonymous=False)
    global out

    out = rospy.Publisher('hackathon/path', Path, queue_size=3)

    rospy.Subscriber('LaserFilterNode/reflexSpurPunkte', PointCloud2, callback)

    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except  rospy.ROSInterruptException:
        pass
