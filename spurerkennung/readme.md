#  HTW -  Hackathon
## Spurerkennung

### Author 
Istvan (istvan@todo.todo)<br>
Jonas Fabich (jonas.fabich@htw-dresden.de)</br>

### Synopsis
Using highly reflective lane points to extract path points to drive on
* uses a y-axis standard deviation filter
* polynomial is limited on x² factor
* build simple polynomial trough extracted points with polyfit


* subscribed topic:
    + /LaserFilterNode/reflexSpurPunkte     #only high reflectivity points
 

* published topics:
    + /hackathon/path   #Extracted path points published as path


### Installation
Download the source in your /catkin/src directory and run catkin_make 


### Run node with:   
`rosrun spurerkennung polynode`  
works with lidar_segmentation example lidar_segmentation_example_node

### Hints / ToDos 
* improve path planning method (simple polynome)
* improve constants with further testing
* improve filter methods
* change polynomial offset method

### Tests
very few