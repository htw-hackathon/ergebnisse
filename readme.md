#  HTW -  Hackathon

## Content

+ 3 working implementations to segmentate lane markings
+ 2 of which publish path points
+ readmes for every package

#### Team 1

+ lidar lane detection and path publication with packages lidar_segmentation_example and spurerkennung
+ image lane detection with additional rollei camera, bildverarbeitung package, no working path publication

#### Team 2

+ lidar lane detection and path extraction with package lane_detection